# Changelog

## 1.0.2

### Restore
- Bugfix: Compression command on restore

## 1.0.1

### General
- Bugfix: Unbound variable when no parameters are supplied

## 1.0.0

### General
- The scripts now exit when any command fails.
- Defined the command for compression in the "TODO section" of the script for easier customization.
- Added section for setup in readme.
- Document requirement pigz when using compression.

### Backup
- Bugfix: Fixed the double trailing slash for paths containing the variable `backupdir`.

## 0.1.2
- Added timestamps for all steps.

## 0.1.1

### General
- User pigz for compression.

## 0.1.0

### General
- First version of Wordpress-Backup-Restore